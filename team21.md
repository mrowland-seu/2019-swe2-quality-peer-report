# Team 21 Feedback

##Last Updated: 11/12/19 at 6:45pm


## CIC

Rating: Green/Great

Overall, this component is pretty strong in terms of organization and documentation.
A sudden newcomer would more easily understand your code. Keep up the good work.

### The Good

--Well documented (Good Commenting within the Code)

--Good Abundance of Tests

--Clean Code

### The Bad

{Nothing to See Here}

### The OK

--Api shows a Heartbeat

## Frontend/Client

Rating: Yellow/OK

In terms of structure, the frontend client is well-defined.
The client page is rather empty though.

DANGER: There is no logout button, which is bad for the UAT. Fix this issue as soon as possible!

### The Good

--Good Route Design

--Clean Structure

### The Bad

--Client is a rather empty and barren desert (It's Empty)

--No Logout Button (Will not pass UAT in its current state)

### The OK

--The Client Side Exists...

## Gateway Controller

Rating: Orange/Unimpressive

It is good that the controller is utilizing sockets and multithreading, however there
are some problems to note.

While there is some documentation in the comments, there needs to be more.

In addition to some questionable coding decisions, the code files for the gateway controller 
and the gateway diagnostics are not organized very well. Perhaps divide the code into repositories 
to help better distinguish between the two components at least. Right now, it is a bit of a mess.

### The Good

--Displays a fine use of sockets and multithreading

### The Bad

--There needs to be a better distinction between the java and python source code

--A While True is present (That is Bad)

--You used a subdomain as an environment variable...not a good practice

### The OK

--There are comments, but they need to be more plentiful and helpful

## Gateway Diagnostics

Rating: Orange/Unimpressive

Like with the gateway controller, this part needs some form of distinction
that helps quality assurance group be able to find the files more easily.

While yes, there is generated data, all of the data seen so far are heartbeats. 
Can we get data that is more related to your industry?

The file used for the diagnostics is also surprisingly small...shouldn't there be a bit more to it?
### The Good

--It uses branching...that's it.

### The Bad

--Worryingly small file

--There needs to be a better distinction between the java and python source code

### The OK

--It generates data, but this data is only a set of heartbeats

--Uses a socket for gateway communication

## Device Simulator

Rating: Orange/Unimpressive

The code in this component has quite a few comments that easy to follow along.

However, the lack of a well-detailed readMe document at the time of the review, 
does hinder one's understanding of the code in general. 
There is also some abiguitywithin the naming of the repository's files. There either needs to
be a better indcator for where to look first (i.e. detail in the readMe where QA reviewers should start),
or there needs to be a change in the names of some files better disguish one file from another.

### The Good

--Good Commenting

### The Bad

--There is a distinctive lack of a ReadMe Document. Please make this file
and put in details in accordance to your team's QA Framework.

--Ambiguous Starting Point for QA Reviewers(The file naming is a bit tough to read at a glance)

### The OK

{Nothing to See Here}

## Documentation

Rating: Yellow/OK



### The Good

--Good Version Documenting (Multiple Vesrions Present)

--Good Diagrams

### The Bad

--Documentation is a little disorganized.

--There are empty folders that are not being used.
----Why are they existing if you're not using them?

### The OK

--Large route structure

--The Documentation Exists....

## DevOps Practices

Rating: Yellow/OK

There is progress and there is deployment, but only half of the repositories are fully functional with CI/CD capabilities
Get those repositories ready as soon you can.

### The Good

--There are deployments

### The Bad

--These repositories need to be ready for deployment.
----Get the CD functionality of these repos ready...

### The OK

--2/4 Repositories Have CI/CD Functionality
--Easy to Port
