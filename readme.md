# SWE2 - QA Peer Analysis

This repository is a writeup of a peer review between teams' QA Leads of all components across each team's system, with a focus on quality analysis of components and the current status of components compared to where they should be for Demo Day. Some things on this list may already be in progress or may have been pointed out already by your QA/DevOps Lead. These are simply results of our findings in an overview, and may not be an exhaustive list. 

## Graphic and Analysis

In class, we provided a graphic representation of each team's projects based on our findings, which can also be found below. This is a very high level overview, and we recommend reading your specific component's notes to get a better picture. There is a lot of qualitative analysis happening behind these results. 

![](results.png)

## Team Breakdown

* [Team 21](team21.md)
* [Team 22](team22.md)
* [Team 23](team23.md)
