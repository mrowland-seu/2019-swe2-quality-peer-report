# Team 22 Feedback

## CIC

### The Good

* Unit testing.
* Routes are protected!

### The Bad

* No request body validation before sending to mongoose.
* Dependencies are manually installed, and not in package.json.

### The OK

* Using 'test' database in MongoBD.
* Tokens being stored to MongoDB.

## Frontend/Client

### The Good

* Routes have authentication required.
* Structured good.

### The Bad

* Some routes are not properly protected.
* Commit messages are unclear.

### The OK

* Code sources are documented. (Youtube, Stack Overflow, etc.)

## Gateway Controller

### The Good

* Good README
* Well Commented
* JUnit testing is present.

### The Bad

* Multiple repos. - Unclear which one is current.
* Root of repo is messy, and includes extraneous files.

### The OK

* N/A

## Gateway Diagnostics

### The Good

* Good README
* Code has headers at the start of each file.

### The Bad

* Nested while(true) loops.
* No testing.

### The OK

* Code seems very sparse.
* Are all features present?

## Device Simulator

### The Good

* Very good variable and method naming.
* Good utilization of git branches.

### The Bad

* No comments at all.
* Experiments / testing left in master branch.

### The OK

* N/A

## Documentation
 
### The Good

* Box directories are very well organized.
* Knowledge transfer documents.
* Class notes.
* Documentation is in depth, and well organized.
* Diagrams are up to date.

### The Bad

* N/A

### The OK

* Digital Ocean passwords are in plain text on Box.

## DevOps Practices

### The Good

* All components are covered with CI/CD

### The Bad

* N/A

### The OK

* MongoDB is exposed to the internet.