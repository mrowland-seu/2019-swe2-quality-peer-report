# Team 23 Feedback

Mostly good practices with some risks identified and some questionable actions. As far as the team is concerned, only some cohesion in terms of following the QA framework, some components are old or lagging behind, and branch names are non-standard and strangely named at times. 

## CIC

### The Good

* Using feature branches
* MongoDB models use default, require, etc as well as type definitions
* Includes request body validation checks 

### The Bad

* Contains broken regular expressions that fail certain methods
* Lacking unit tests/unit test coverage

### The OK

* Has commented out code blocks
* Some comments, but not many

## Frontend/Client

### The Good

* Separation of concerns between user authentication & client operations and CIC
* URI changes based on environment variables :D

### The Bad

* Using the MongoDB "test" db
* Messy folder structure
* Lots of `.DS_Store` files in repository that shoudn't be there

### The OK

* "some" testing, but not nearly a full suite
* Comments are present, but not always necessarily helpful 

## Gateway Controller & Gateway Diagnostics
Team 23's GW Controller and Diagnostics modules live in the same repository, and are therefore treated as one section for the purposes of this review.

### The Good

* Very good documentation
* Large suite of unit tests
* Defined separation of concerns well 
* Good comments
* File Headers inlne with QA Framework
* Error handling

### The Bad

### The OK

* Move main method to it's own file, hard to find it
* Not using environment variables for URIs 

## Device Simulator

### The Good

* Good documentation

### The Bad

* Doesn't appear to be using branching at all
* Minimal Commits
* No update to the repository in some time

### The OK

* Seems to be too small/too simple? for what a data simulator should be
* Technical Debt: Only using prefabricated data, not generating its own on the fly

## Documentation
 
### The Good

* Diagrams being kept up to date
* Risk management is comprehensive

### The Bad

* Roadmaps not up to date

### The OK

* Structure is good, but could potentially be consolidated
* Couldn't find component diagrams for GW and CIC

## DevOps Practices

### The Good

* Automated Scripting 

### The Bad

### The OK

* Only one repo currently configured for CI/CD, but configuration easily ported
* Some documentation
